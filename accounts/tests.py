from django.test import LiveServerTestCase
from django.urls import reverse_lazy
from selenium.webdriver.chrome.webdriver import WebDriver

class TestLogin(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.selenium = WebDriver(executable_path='/bin/chromedriver')

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def testLogin(self):
        # ログインページを開く
        self.selenium.get('http://localhost:8000' + str(reverse_lazy('account_login')))

        # ログイン
        username_input = self.selenium.find_element_by_name("login")
        username_input.send_keys('yoshiki7878@gmail.com')

        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys("tnkyshk000")

        self.selenium.find_element_by_class_name('btn').click()

        self.assertEquals('日記一覧 | Private Diary', self.selenium.title)

